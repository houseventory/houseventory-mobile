import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { MyApp } from './app.component';

import { HousesPage } from '../pages/houses/houses';
import { HouseDetailsPage, HouseDetailsPopover } from '../pages/house-details/house-details';
import { HouseFormModal } from '../pages/house-form/house-form';

import { HouseData } from '../providers/data/house-data';

@NgModule({
  declarations: [
    MyApp,
    HousesPage,
    HouseDetailsPage,
    HouseFormModal,
    HouseDetailsPopover
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HousesPage,
    HouseDetailsPage,
    HouseFormModal,
    HouseDetailsPopover
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    File,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    HouseData
  ]
})
export class AppModule {}
