export class House {
  public _id: string;
  public _rev: string;
  public _attachments: any;

  public name: string;
  public address: string;

  constructor(obj: any) {
    if (obj == null) {
      this._id = null;
      this._rev = null;
      this._attachments = null;
      this.name = null;
      this.address = null;
    } else {
      this._id = obj._id;
      this._rev = obj._rev;
      this.name = obj.name;
      this.address = obj.address;
      this._attachments = obj._attachments;
    }
  }

  addPhoto(data) {
    this._attachments = {
      "photo" : {
        content_type: "text/plain",
        data: data
      }
    };
  }
};
