import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, PopoverController, ViewController } from 'ionic-angular';
import { HouseFormModal } from '../house-form/house-form';
import { House }  from '../../models/house';
import { HouseData } from '../../providers/data/house-data'

@Component({
  template: `
    <button ion-item (click)="delete()">Delete</button>
  `
})
export class HouseDetailsPopover {
  constructor(public viewCtrl: ViewController) {}

  delete() {
    this.viewCtrl.dismiss("delete");
  }
}






@IonicPage()
@Component({
  selector: 'page-house-details',
  templateUrl: 'house-details.html',
})
export class HouseDetailsPage {
  selectedHouse: House;

  constructor(public navCtrl: NavController, public navParams: NavParams, public houseData: HouseData, public modalCtrl: ModalController, public popoverCtrl: PopoverController) {
    this.selectedHouse = this.navParams.get('house');
  }

  editHouse(event, house: House) {
    let modal = this.modalCtrl.create(HouseFormModal, {house: this.selectedHouse});
    modal.onDidDismiss((dismissedHouse) => {
      if (dismissedHouse != null) {
        this.houseData.get(this.selectedHouse._id).then((h: House) => {
          this.selectedHouse = h;
        });
      }
    });
    modal.present();
  }

  openMoreMenu(event) {
    let popover = this.popoverCtrl.create(HouseDetailsPopover);
    popover.present({
      ev: event
    });
    popover.onDidDismiss((action) => {
      if (action == "delete") {
        this.houseData.delete(this.selectedHouse);
        this.navCtrl.pop();
      }
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HousedetailsPage');
  }
}
