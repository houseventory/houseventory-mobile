import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HouseFormModal } from './house-form';

@NgModule({
  declarations: [
    HouseFormModal,
  ],
  imports: [
    IonicPageModule.forChild(HouseFormModal),
  ],
  entryComponents: [
    HouseFormModal,
  ]
})
export class HouseFormModalModule {}
