import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ViewController, NavParams, AlertController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { House }  from '../../models/house';
import { HouseData } from '../../providers/data/house-data'

@Component({
  templateUrl: 'house-form.html',
})

export class HouseFormModal {
  formData: any;
  action: string;
  photoData: string = null;

  constructor(public viewCtrl: ViewController, public navParams: NavParams, public formBuilder: FormBuilder, public houseData: HouseData, private alertCtrl: AlertController, public camera: Camera, public file: File) {
    let house = navParams.get('house');

    this.action = (house._id == null) ? 'new' : 'edit';

    this.formData = formBuilder.group({
      name: [house.name, Validators.required],
      address: [house.address, Validators.required],
      _id: [house._id],
      _rev: [house._rev]
    });
  }

  save() {
    if (!this.formData.valid || !this.photoData) {
      let alert = this.alertCtrl.create({
        title: 'Errors',
        subTitle: 'All fields are required!',
        buttons: ['OK']
      });
      alert.present();
    } else {
      let h = new House(this.formData.value);
      h.addPhoto(this.photoData);
      if (this.action == "new") {
        this.houseData.add(h);
      } else {
        this.houseData.update(h);
      }
      this.viewCtrl.dismiss(h);
    }
  }

  dismiss() {
    this.viewCtrl.dismiss(null);
  }

  takePhoto(event) {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };

    this.camera.getPicture(options).then((imageData) => {
      this.photoData = imageData;
    }, (err) => {
      console.log(err);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HouseFormPage');
  }
};
