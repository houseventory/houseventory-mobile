import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { HouseDetailsPage } from '../house-details/house-details';
import { HouseFormModal} from '../house-form/house-form';
import { House } from '../../models/house';
import { HouseData } from '../../providers/data/house-data'

@IonicPage()
@Component({
  selector: 'page-houses',
  templateUrl: 'houses.html',
})
export class HousesPage {
  houses: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public houseData: HouseData, public modalCtrl: ModalController) {
    this.houseData.getAll().then((houses) => {this.houses = houses;});
  }

  selectHouse(event, house: House) {
    this.navCtrl.push(HouseDetailsPage, {
      house: house
    });
  }

  newHouse(event) {
    let modal = this.modalCtrl.create(HouseFormModal, {house: new House(null)});
    modal.onDidDismiss((house) => {
      if (house != null) this.houseData.getAll().then((houses: Array<House>) => {this.houses = houses});
    });
    modal.present();
  }

  doRefresh(event) {
    this.houseData.getAll().then((houses) => {this.houses = houses});
    event.complete();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HousesPage');
  }
}
