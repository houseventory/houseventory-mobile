import { Injectable, NgZone } from '@angular/core';
import { House } from '../../models/house'
import PouchDB from 'pouchdb';
import PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);

@Injectable()
export class HouseData {
  data: Array<House>;

  db: any;
  remoteUser: string;
  remotePassword: string;
  remoteDB: string;

  constructor() {
    this.db = new PouchDB('houseventory-house');
    this.db.createIndex({
      index: {fields: ['name', '_id']}
    });

    this.remoteDB = "http://couchdb.houseventory.io:5984/houseventory-house";
    this.remoteUser = "admin";
    this.remotePassword = "admin";

    let options = {
      live: true,
      retry: true,
      continous: true,
      auth: {
        username: this.remoteUser,
        password: this.remotePassword
      }
    };

    this.db.sync(this.remoteDB, options);
  }

  add(house) {
    console.log(house);
    this.db.post(house);
  }

  update(house) {
    this.db.put(house).then((response) => {
      console.log(response);
    });
  }

  get(id) {
    return new Promise((resolve) => {
      this.db.find({
        selector: {
          _id: id
        }
      }).then((result) => {
        resolve(new House(result.docs[0]));
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  getAll() {
    return new Promise((resolve) => {
      this.db.find({
        selector: {
          name: {$gte:null}
        },
        sort: ['name'],
        attachments: true
      }).then((result) => {
        console.log(result);
        this.data = [];
        result.docs.map((house) => {this.data.push(new House(house))});
        resolve(this.data);
      }).catch((error) => {
        console.log(error);
      });
    });
  }

  delete(house) {
    this.db.remove(house);
  }
}
